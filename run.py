#!/usr/bin/env python
#============================================================================
# This is the Python script of running the following steps of the simulation:
# [1] generating input files (power spectrum, 2-point correaltion function and
#     the power-spectrum of the log-normal field)
# [2] generating mock galaxy catalog from the input files calculated in [1]
# [3] calculating the power spectrum from the mock galaxy catalog generated in [2]
#
# To run the code, type:
#
# > ./run.py example.ini
#
# where exmaple.ini is the exmaple of input file.
# It contains the input parameters of simulation e.g. cosmological parameters,
# survey geometries, etc.
#
# The resulting files are as follows:
# 1) input files
# ./data/inputs/example_pk.txt
# ./data/inputs/exmaple_pkG_b1.5.txt
# ...
# 2) mock galaxy catalog
# For each realization, the code generates the followng two files:
# ./data/lognormal/example_lognormal_rlz{# of realization}.bin (for galaxy field)
# ./data/lognormal/example_density_lognormal_rlz{# of realization}.bin (for density field)
# 3) Power spectrum
# ./data/pk/exmaple_pk_rlz{# of realization}.dat
# The column 1, 2, 3 and 4 are k [Mpc/h], P_0, P_2 and P_4 [Mpc^3/h^3], respectively.
#
# For the detailes of the calculations of each step, please see readme.txt.
#
# 04 Sep 2016
# by Ryu Makiya
#============================================================================

import sys
import random
import numpy as np
from multiprocessing import Manager
from multiprocessing import Pool
from multiprocessing import Process
from run_module import *

if __name__ == '__main__':
	# objects shared between processes
	manager = Manager()
	params = manager.list()
	seed1 = manager.list()
	seed2 = manager.list()
	seed3 = manager.list()

	# read parameters from .ini file
	ini_file_name = sys.argv[1]
	params = read_params(ini_file_name)

	# check whether output directory exists or not
	check_dir(params)

	# create seeds for random
	random.seed(int(params['seed']))
	seed1 = []
	seed2 = []
	seed3 = []

	for i in range(0,params['Nrealization']):
		seed1.append(random.randint(1,100000))
		seed2.append(random.randint(1,100000))
		seed3.append(random.randint(1,100000))

	# class for execute c++ and fortran codes
	exe = executable("exe")

	# generate input files
	if params['gen_inputs']:
		gen_inputs(params,exe)
	else:
		print('skip: generating input files')

	pools = Pool(params['num_para'])
	# generate Poisson catalog
	if params['run_lognormal']:
		args = [(i,params,seed1,seed2,seed3,exe) for i in range(params['Nrealization'])]
		run = pools.map(wrap_gen_Poisson,args)
	else:
		print('skip: generating log-normal catalog')

	# calculate Pk
	if params['calc_pk']:
		args = [(i,params,exe) for i in range(params['Nrealization'])]
		run = pools.map(wrap_calc_Pk,args)
	else:
		print('skip: calculate Pk')

	# calculate Pk
	if params['calc_cpk']:
		args = [(i,params,exe) for i in range(params['Nrealization'])]
		run = pools.map(wrap_calc_cPk,args)
	else:
		print('skip: calculate cross Pk')
